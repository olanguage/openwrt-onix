OpenWRT Repository for O Language
-------------------------------------

Get OpenWRT;

```
git clone https://github.com/openwrt/openwrt.git
cd openwrt
./scripts/feeds update -a
./scripts/feeds install -a
```

Add to feeds.conf file append;

```
nano feeds.conf  
```

Add end line;
```
src-git onix https://gitlab.com/olanguage/openwrt-onix.git
```

Update feeds
```
./scripts/feeds update onix
./scripts/feeds install -a -p olang
```


Open menu and find olang and install
```
make menuconfig
```


Build openwrt;
```
make <build options>
```